# BRCodigo Softwares do Brasil  - maven repository

Seja bem-vindo ao privado repositório de artefatos da BRCodigo. Você pode usar com o maven, sbt, ivy ou outros gerenciadores de dependências compatíveis.

## Uso com **maven**
## **DOWNLOAD DE DEPENDENCIAS**

No POM do projeto ```pom.xml```, adicione o código abaixo:
  
```
    <repositories>
        <repository>
            <id>brcodigo-bitbucket</id>
            <name>BRCodigo maven repository</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <url>https://api.bitbucket.org/1.0/repositories/brcodigo/maven/raw/master</url>
        </repository>
    </repositories>

```
  

No maven settings ```settings.xml```, adicione o código abaixo:
  
```
<settings>
    <servers>
        <server>
            <id>brcodigo-bitbucket</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Authorization</name>
                        <!-- Base64-encoded "Basic bitbucketuser:password" Exemple: Basic YnJoZW5yaXF1ZTpraW5ob2ZlbGl4 -->
                        <value>Basic YnJoZW5yaXF1ZTpraW5ob2ZlbGl4</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>

```

## **DEPLOY DE DEPENDENCIAS**

No POM do projeto ```pom.xml```, adicione o código abaixo:

      <distributionManagement>
        <snapshotRepository>
            <id>repo-id</id>
            <name>my snapshot repository</name>
            <url>file://D:\projetos\maven</url>
        </snapshotRepository>
        <repository>
            <id>repo-id2</id>
            <name>my repository</name>
            <url>file://D:\projetos\maven</url>
        </repository>
    </distributionManagement>